import java.io.IOException;

/**
 * Данный класс содержит метод main, который последовательно вызывает методы main в классах
 * TranslateNumbers, FilterIntNumbers, VerifyNumberForLuck.
 * @author Малякин Кирилл. Гр. 15ОИТ20.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        TranslateNumbers.main(args);
        FilterIntNumbers.main(args);
        VerifyNumberForLuck.main(args);
        System.out.println("--------------------------");
        System.out.println("All procedures complete");
    }
}
