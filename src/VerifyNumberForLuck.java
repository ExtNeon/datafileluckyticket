import java.io.*;

/**
 * Данный класс содержит метод main, который подгружает отфильтрованный список чисел из файла int6data,
 * и проверяет, удачны ли они (сумма первых трёх цифр = сумме последних трёх цифр)
 * @author Малякин Кирилл. Гр. 15ОИТ20.
 */
public class VerifyNumberForLuck {
    public static void main(String[] args) throws IOException {
        DataInputStream numbers = new DataInputStream(new FileInputStream("int6data.dat"));
        PrintWriter luckyNumbers = new PrintWriter(new FileWriter("txt6data.dat"));
        while (numbers.available() > 0) {
            try {
                String readedNum = "" + numbers.readInt();
                if (isLucky(expandString(readedNum, 6, '0', false))) {
                    luckyNumbers.println(expandString(readedNum, 6, '0', false));
                } else {
                    if (isLucky(expandString(readedNum, 6, '0', true))) {
                        luckyNumbers.println(readedNum);
                    }
                }
            } catch (IOException e) {
                numbers.close();
                luckyNumbers.close();
                e.printStackTrace();
            }
        }
        numbers.close();
        luckyNumbers.close();
        System.out.println("Lucky numbers extraction complete");
    }

    /**
     * Возвращает строку длиной @code{targetLength}, содержание которой будет разным в зависимости от параметров.
     *
     * <ol> <li> 1) Если длина @code{inputString} меньше длины @code{targetLength}, то результат = @code{inputString}
     *          + некоторое количество символов @code{fillChar}, дополняющих строку до @code{targetLength}
     *      <li> 2) Если длина @code{inputString} равна @code{targetLength}, то возвращается @code{inputString}
     *      <li> 3) Если длина @code{inputString} больше @code{targetLength}, то возвращается подстрока от @code{inputString},
     *          длина которой равна @code{targetLength}
     * </ol>
     *
     * @param inputString Исходная строка. На её основе будет построена результирующая строка.
     * @param targetLength Целевая длина строки. От этого параметра зависит длина результирующей строки
     * @param fillChar Символ, которым в случае <i>1</i> будет дополнена строка @code{inputString}
     * @param toEnd Параметр, отвечающий за то, в какую часть строки будут помещены символы @code{fillChar} в случае <i>1</i>.
     *              Если @code{true}, то в конец, иначе в начало.
     * @return Строка длиной @code{targetLength}, основанная на строке @code{inputString}, дополненная символами @code{fillChar},
     * или урезанная, в случае, если длина исходной строки @code{inputString} больше @code{targetLength}.
     */
    private static String expandString(String inputString, int targetLength, char fillChar, boolean toEnd) {
        StringBuilder stringBuilder = new StringBuilder();
        if (inputString.length() == targetLength) {
            return inputString;
        } else {
            if (inputString.length() > targetLength) {
                return inputString.substring(0, ++targetLength);
            }
        }
        if (toEnd) {
            stringBuilder.append(inputString);
            while (stringBuilder.length() < targetLength) {
                stringBuilder.append(fillChar);
            }
        } else {
            targetLength -= inputString.length();
            while (stringBuilder.length() < targetLength) {
                stringBuilder.append(fillChar);
            }
            stringBuilder.append(inputString);
        }
        return stringBuilder.toString();
    }

    /**
     * Возвращает булевое значение, свидетельствующее о том, удачен ли номер @code{input}, или же нет.
     * Удачным номером называется такое шестизначное число, сумма первых трёх цифр которого равна сумме оставшихся трёх цифр.
     * @param input Входящий номер, который необходимо проверить. В качестве типа параметра используется строка,
     *              так как счастливый номер может иметь нули в своём начале, передача которых невозможна
     *              в случае использования типа integer.
     * @return булевое значение, равное @code{true}, если номер удачен, и @code{false} в противном случае.
     */
    private static boolean isLucky(String input) {
        int firstSum = 0;
        int secondSum = 0;
        for (int i = 0; i < 3; i++) {
            firstSum += Integer.valueOf(String.valueOf(input.charAt(i)));
        }
        for (int i = 3; i < 6; i++) {
            secondSum += Integer.valueOf(String.valueOf(input.charAt(i)));
        }
        return firstSum == secondSum;
    }
}
