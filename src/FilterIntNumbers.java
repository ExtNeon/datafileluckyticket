import java.io.*;

/**
 * Данный класс содержит метод main, который подгружает список чисел, хранящихся в бинарной форме в файле intdata.dat,
 * и отфильтровывает их, пропуская только те числа, количество цифр в которых больше 3, но меньше 7. Полученные числа он
 * сохраняет в бинарный файл int6data.dat
 * @author Малякин Кирилл. Гр. 15ОИТ20.
 */
public class FilterIntNumbers {

    public static void main(String[] args) throws IOException {
        DataInputStream numbersFile = new DataInputStream(new FileInputStream("intdata.dat"));
        DataOutputStream sixDigitNumbersFile = new DataOutputStream(new FileOutputStream("int6data.dat"));
        try {
            while (numbersFile.available() > 0) {
                int currentNumber = numbersFile.readInt();
                if (currentNumber > 999 && currentNumber < 1000000) {
                    sixDigitNumbersFile.writeInt(currentNumber);
                }
            }
        } catch (IOException e) {
            numbersFile.close();
            sixDigitNumbersFile.close();
            e.printStackTrace();
        }
        numbersFile.close();
        sixDigitNumbersFile.close();
        System.out.println("Filtering complete");
    }

    /*
        /**
         * Возвращает количество цифр в числе @code{inputNumber}.
         * @param inputNumber Исходное число
         * @return Количество цифр в числе @code{inputNumber}.

        private static int getCountOfDigits(int inputNumber) {
            int result = 1;
            while (inputNumber / 10 > 0) {
                result++;
                inputNumber /= 10;
            }
            return result;
        }

     */
}
