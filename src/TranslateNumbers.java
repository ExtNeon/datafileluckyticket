import java.io.*;

/**
 * Данный класс содержит метод main, который подгружает список чисел, хранящихся
 * в текстовой форме (каждое новое число находится на новой строке) в файле inputNumbers.txt,
 * и переводит их в бинарную форму, сохраняя в файл intdata.dat.
 * @author Малякин Кирилл. Гр. 15ОИТ20.
 */
public class TranslateNumbers {
    public static void main(String[] args) throws IOException {
        BufferedReader txtFile = new BufferedReader(new FileReader("inputNumbers.txt"));
        DataOutputStream datFile = new DataOutputStream(new FileOutputStream("intdata.dat"));
        String line;
        try {
            while ((line = txtFile.readLine()) != null) {
                datFile.writeInt(Integer.valueOf(line));
            }
        } catch (IOException e) {
            datFile.close();
            txtFile.close();
            e.printStackTrace();
        }
        datFile.close();
        txtFile.close();
        System.out.println("Translation complete");
    }
}
